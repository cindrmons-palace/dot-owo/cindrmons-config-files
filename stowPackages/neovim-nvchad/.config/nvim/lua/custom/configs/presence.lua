require("presence").setup({
  auto_update = true,
  neovim_image_text = "Me Me Big Boy",  -- Text displayed when hovering over neovim logo
	main_image			= "file",						  -- main image display (either "neovim" or "file")
  enable_line_number  = true,           -- Displays the current line number instead of the current project
  buttons = true,                       -- configure rich presence buttons
})
