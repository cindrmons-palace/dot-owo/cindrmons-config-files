local on_attach = require("plugins.configs.lspconfig").on_attach
local capabilities = require("plugins.configs.lspconfig").capabilities

local lspconfig = require "lspconfig"
-- local servers = { "ansible-language-server", "cssls", "clangd"}

lspconfig.ansiblels.setup({
  on_attach = on_attach,
  capabilities = capabilities,
  filetypes = {"yaml.ansible"},
  root_dir = lspconfig.util.root_pattern("ansible.cfg","run.yml"),
})

lspconfig.gopls.setup({
  on_attach = on_attach,
  capabilities = capabilities,
})

lspconfig.graphql.setup({
  on_attach = on_attach,
  capabilities = capabilities,
  filetypes = {"grpahql", "graphqls", "typescriptreact", "javascriptreact"},
})

lspconfig.rust_analyzer.setup({
  on_attach = on_attach,
  capabilities = capabilities,
})

lspconfig.docker_compose_language_service.setup({
  on_attach = on_attach,
  capabilities = capabilities,
})

lspconfig.dockerls.setup({
  on_attach = on_attach,
  capabilities = capabilities,
})

-- lspconfig.yamlls.setup({
--   on_attach = on_attach,
--   capabilities = capabilities,
--   root_dir = lspconfig.util.root_pattern(".yamllint"),
-- })

-- for _, lsp in ipairs(servers) do
--   lspconfig[lsp].setup {
--     on_attach = on_attach,
--     capabilities = capabilities,
--   }
-- end
