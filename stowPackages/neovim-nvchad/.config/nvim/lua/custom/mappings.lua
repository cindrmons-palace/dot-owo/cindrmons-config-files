local M = {}

-- M.disabled = {
--   n = {
--     ["<A-i>"] = "",
--   },
--   t = {
--     ["<A-i>"] = "",
--   }
-- }

M.general_tmux = {
  n = {
    ["<C-h>"] = { "<cmd> TmuxNavigateLeft<CR>", "Tmux: Window Left"},
    ["<C-l>"] = { "<cmd> TmuxNavigateRight<CR>", "Tmux: Window Right"},
    ["<C-j>"] = { "<cmd> TmuxNavigateDown<CR>", "Tmux: Window Down"},
    ["<C-k>"] = { "<cmd> TmuxNavigateUp<CR>", "Tmux: Window Up"},
  }
}

return M
