-- Allows Neovim to access System Clipboard

-- EDITOR {{{
vim.opt.showmatch = true              -- show matching bracket
vim.opt.relativenumber = true         -- show relative numbered lines
-- }}}

-- EXTERNAL INPUT {{{
vim.opt.clipboard = "unnamedplus"     -- allows neovim to access the system clipboard
-- }}}

-- CURSOR POSITION {{{
vim.opt.cursorline = true             -- highlights current line
vim.opt.cursorcolumn = true           -- highlights current column
-- }}}

-- DISABLE NEOVIM BACKGROUND {{{
-- VIM API
local api = vim.api
local autocmd = api.nvim_create_autocmd
local augroup = api.nvim_create_augroup

local function augroupc(group_name)
	augroup(group_name, {clear = true})
end

-- DISABLE NEOVIM BACKGROUND
augroupc("disable_background")

-- Disable Neovim Background
autocmd('VimEnter',
{
	pattern = "*",
	group = "disable_background",
	command = "highlight Normal guibg=NONE ctermbg=NONE"
})

-- keep colorscheme background color for cursor column and line instead 
autocmd('VimEnter',
{
	pattern = "*",
	group = "disable_background",
	command = "highlight! link CursorColumn CursorLine"
})

-- }}}

-- vim:fileencoding=utf-8:foldmethod=marker
