-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  -- Plugins go beyond this line --
  
  -- Catppuccin.nvim
  -- https://github.com/catppuccin/nvim
  use { 
	  "catppuccin/nvim", 
	  as = "catppuccin",
	  config = function()
		  vim.cmd.colorscheme "catppuccin"
	  end
  }

  -- Nvim-Treesitter
  -- https://github.com/nvim-treesitter/nvim-treesitter
  use {
	  'nvim-treesitter/nvim-treesitter',
	  run = function()
		  local ts_update = require('nvim-treesitter.install').update({ with_sync = true })
		  ts_update()
	  end
  }

  -- LSP-Zero
  -- https://github.com/VonHeikemen/lsp-zero.nvim
  use {
	  'VonHeikemen/lsp-zero.nvim',
	  requires = {
		  -- LSP Support
		  {'neovim/nvim-lspconfig'},
		  {'williamboman/mason.nvim'},
		  {'williamboman/mason-lspconfig.nvim'},

		  -- Autocompletion
		  {'hrsh7th/nvim-cmp'},
		  {'hrsh7th/cmp-buffer'},
		  {'hrsh7th/cmp-path'},
		  {'saadparwaiz1/cmp_luasnip'},
		  {'hrsh7th/cmp-nvim-lsp'},
		  {'hrsh7th/cmp-nvim-lua'},

		  -- Snippets
		  {'L3MON4D3/LuaSnip'},
		  {'rafamadriz/friendly-snippets'},
	  }
  }

  -- Telescope.nvim
  -- https://github.com/nvim-telescope/telescope.nvim
  use {
	  'nvim-telescope/telescope.nvim', tag = '0.1.0',
	  -- or                            , branch = '0.1.x',
	  requires = { {'nvim-lua/plenary.nvim'} }
  }

  -- Harpoon
  -- https://github.com/ThePrimeagen/harpoon
  use {
	  'ThePrimeagen/harpoon',
	  requires = { {'nvim-lua/plenary.nvim'} }
  }

  -- Undo-Tree
  -- https://github.com/mbbill/undotree
  use {
	'mbbill/undotree'
  }

  -- Vim-Fugitive
  -- https://github.com/tpope/vim-fugitive
  use {
	  'tpope/vim-fugitive'
  }

  -- presence.nvim
  -- https://github.com/andweeb/presence.nvim
  use {
	  'andweeb/presence.nvim'
  }

  -- lsp-format.nvim
  -- https://github.com/lukas-reineke/lsp-format.nvim
  use {
	  "lukas-reineke/lsp-format.nvim"
  }

  -- Discord.nvim
  -- https://github.com/aurieh/discord.nvim
  use {
	  "https://github.com/aurieh/discord.nvim"
  }
  -- --------------------------- --
end)
