-- KEYMAP OPTIONS {{{
local opts = { noremap = true, silent = true }
local term_opts = { silent = true }
-- }}}

-- LOCAL CONSTS {{{
local kmap = vim.api.nvim_set_keymap
-- }}}


-- LEADER KEY
vim.g.mapleader = " "

-- Exit out of file in nvim
vim.keymap.set("n", "<leader>q", vim.cmd.Ex)

-- BETTER WINDOW NAVIGATION {{{
kmap("n", "<C-h>", "<C-w>h", opts)
kmap("n", "<C-j>", "<C-w>j", opts)
kmap("n", "<C-k>", "<C-w>k", opts)
kmap("n", "<C-l>", "<C-w>l", opts)
--- }}}

-- BETTER FOLDS {{{
kmap("n", "<S-Tab>", "za", opts)
-- }}}

-- WINDOW MANAGEMENT {{{
kmap("n", "<leader>wh", ":split<cr>", opts)
kmap("n", "<leader>wv", ":vsplit<cr>", opts)
kmap("n", "<leader>wc", ":close<cr>", opts)
-- }}}

-- BUFFER MANAGEMENT {{{
kmap("n", "<S-l>", ":bnext<cr>", opts)
kmap("n", "<S-h>", ":bprevious<cr>", opts)
-- }}}

-- TAB MANAGEMENT {{{
--kmap("n", "<leader>n", ":tabnew<cr>", opts)
--kmap("n", "<leader>h", ":tabprevious<cr>", opts)
--kmap("n", "<leader>l", ":tabnext<cr>", opts)
--kmap("n", "<leader>c", ":tabclose<cr>", opts)
-- }}}

-- NEOVIM EXPLORER {{{
kmap("n", "<A-v>", ":Lex 30<cr>", opts)
-- }}}

-- CUSTOM VISUAL COMMANDS {{{
	-- Stay in Indent Mode {{{
	kmap("v", "<", "<gv", opts)
	kmap("v", ">", ">gv", opts)
	-- }}}
	
	-- Now I wouldn't need to worry what's on the paste register anymore! {{{
	kmap("v", "p", '"_dP', opts)
	-- }}}
-- }}}

-- PLUGIN KEYMAPS {{{

-- LSP
kmap("n", "<leader>f", ":Format<cr>", opts)

